package org.batwan.arbitrage.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.batwan.arbitrage.model.domain.CurrencyExRate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class CurrencyUtils {
	private static final Logger LOGGER = LoggerFactory.getLogger(CurrencyUtils.class);
	
	private static final String YAHOO_API_CURRENCY_TEMPLATE = 
			"http://query.yahooapis.com/v1/public/yql?q=select%%20*%%20from%%20yahoo.finance.xchange%%20where%%20pair%%20in%%20(%s)&format=json&env=store://datatables.org/alltableswithkeys";
	
	public static final String TO_CURRENCY = "CLP,CNH,CNY,COP,CUP,CZK,DEM,DKK,DZD,EGP,ESP,EUR,FIM,FRF,GBP,GHC,GRD,GTQ,HKD,HRK,HUF,IDR,IEP,ILS,INR,ISK,ITL,JMD,JOD,JPY,KES,KRW,KWD,KZT,LAK,LBP,LKR,LTL,LVL,MAD,MMK,MNT,MOP,MUR,MXN,MYR,NLG,NOK,NZD,OMR,PAB,PEN,PHP,PKR,PLN,PTE,PYG,QAR,RON,RUB,SAR,SEK,SGD,SOS,THB,TND,TRY,TWD,TZS,UYU,VND,ZAR,ZWD";
	private static final List<String> TO_CURRENCY_LIST;
	private static final String DEFAULT_TARGET_CURRENCY = "USD";
	
	static {
		TO_CURRENCY_LIST = Arrays.asList(TO_CURRENCY.split(","));
	}
	
	private static final int DEFAULT_READ_TIMEOUT = 30 * 1000;
	
	private static String getContentByUrlConnection(String url) throws IOException {
		URLConnection connection = new URL(url).openConnection();
		connection.setReadTimeout(DEFAULT_READ_TIMEOUT);
		BufferedReader reader    = new BufferedReader(new InputStreamReader(connection.getInputStream()));
		StringWriter writer      = new StringWriter();
		
		IOUtils.copy(reader, writer);
		
		reader.close();
		
		return writer.toString();
	}
	
	private static String assembleCurrencyPairs(String target, List<String> to) {
		StringBuilder sBuilder = new StringBuilder();
		
		boolean first = true;
		for (String t : to) {
			if (first) first = false;
			else
				sBuilder.append(",");
			sBuilder.append("\"");
			sBuilder.append(target);
			sBuilder.append(t);
			sBuilder.append("\"");
		}
		
		return sBuilder.toString();
	}
	
	public static CurrencyExRate getCurrencyExRateFromYahooAPI() throws MalformedURLException, IOException {
		return getCurrencyExRateFromYahooAPI(DEFAULT_TARGET_CURRENCY, TO_CURRENCY_LIST);
	}
	
	/**
	 * 
	 * @param to The currencies to be query as exchanging from USD.
	 * @return
	 * @throws MalformedURLException
	 * @throws IOException
	 */
	public static CurrencyExRate getCurrencyExRateFromYahooAPI(List<String> to) throws MalformedURLException, IOException {
		return getCurrencyExRateFromYahooAPI(DEFAULT_TARGET_CURRENCY, to);
	}
	
	public static CurrencyExRate getCurrencyExRateFromYahooAPI(String targetCurr, List<String> to) throws MalformedURLException, IOException {
		LOGGER.info("Fetch currency exchange rate (FX) from Yahoo finance API.");
		String url = String.format(YAHOO_API_CURRENCY_TEMPLATE, URLEncoder.encode(assembleCurrencyPairs(targetCurr, to), "UTF-8"));
		
		LOGGER.debug("Connect to Yahoo finance API: {}", url);
		
		String responseJson = getContentByUrlConnection(url);
		LOGGER.debug("Respone from Yahoo api: {}", responseJson);
		
		JsonObject response = JsonUtils.parseJsonStrToJsonObject(responseJson);
//		JsonObject exRates = new JsonObject();
//		Map<String, BigDecimal> exRates = new TreeMap<String, BigDecimal>();
		CurrencyExRate exRate = new CurrencyExRate();
		exRate.setCurrency(targetCurr);
		if (response.has("query")) {
			JsonObject query = response.get("query").getAsJsonObject();
			if (query.has("results")) {
				JsonObject results = query.get("results").getAsJsonObject();
				if (results.has("rate")) {
					JsonArray rates = results.get("rate").getAsJsonArray();
					for (JsonElement element : rates) {
						JsonObject rate = element.getAsJsonObject();
						if (rate.has("Name") && "N/A".equals(rate.get("Name").getAsString()))
							continue;
						String currName = StringUtils.substringAfter(rate.get("Name").getAsString(), "/");
//						exRates.put(currName, new BigDecimal(Double.parseDouble(rate.get("Rate").getAsString())));
						exRate.put(currName, new BigDecimal(Double.parseDouble(rate.get("Rate").getAsString())));
					}
				}
			}
		}
		
//		return exRates;
		return exRate;
	}
}
