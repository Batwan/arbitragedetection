package org.batwan.arbitrage.model.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Map;
import java.util.TreeMap;

import com.google.common.collect.ComparisonChain;
import com.google.common.collect.Ordering;

public class CurrencyExRate implements Cloneable, Serializable, Comparable<CurrencyExRate> {
	private static final long serialVersionUID = 1L;
	
	private String currency;
	private Map<String, BigDecimal> exRates;

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public Map<String, BigDecimal> getExRates() {
//		return exRates;
		return new TreeMap<>(exRates);
	}

	public void setExRates(Map<String, BigDecimal> exRates) {
		this.exRates = exRates;
	}
	
	public CurrencyExRate() {
		exRates = new TreeMap<String, BigDecimal>();
	}
	
	public CurrencyExRate(Map<String, BigDecimal> exRates) {
		this.exRates = exRates;
	}
	
	@Override
	public int compareTo(CurrencyExRate other) {
		return ComparisonChain.start()
				.compare(this.getCurrency(), other.getCurrency(), Ordering.natural().nullsLast())
				.result();
	}
	
	public void put(String currency, BigDecimal exRate) {
		exRates.put(currency, exRate);
	}
	
	public void remove(String currency) {
		exRates.remove(currency);
	}
	
}
